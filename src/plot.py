from const import *
import sys

def init(xlabel, ylabel):
    sys.stdout.write(f"""
    set terminal pdf font "Sans-Serif,7"
    set output "{PLOT_FILE}"

    set style fill transparent solid 0.5 noborder
    set style circle radius 0.08

    set palette maxcolors 4
    set palette defined(  \\
        0 "{R_REJECTED}"  , \\
        1 "{R_ADMITTED}"  , \\
        2 "{W_REJECTED}", \\
        3 "{W_ADMITTED}"  )
    unset colorbox

    set key outside
    set key Left reverse

    set xlabel "{xlabel}"
    set ylabel "{ylabel}"
    """)

def plot( xlabel, ylabel,
          nbStudents, numerusClausus, accuracy,
          features, labels, predictions ):
    accuracy = int(round(accuracy*100))
    
    title  = ""
    title += f"nbStudents = {nbStudents}\\n"
    title += f"numerusClausus = {numerusClausus}\\n"
    title += f"accuracy = {accuracy}%\\n"
    
    cmd = f"""
    
    set key title "{title}" left
    plot "-" using 1:2:3 with circles linecolor palette notitle, \\
         NaN with circles linecolor "{R_ADMITTED}" title "admitted (right)", \\
         NaN with circles linecolor "{R_REJECTED}" title "rejected (right)", \\
         NaN with circles linecolor "{W_ADMITTED}" title "admitted (wrong)", \\
         NaN with circles linecolor "{W_REJECTED}" title "rejected (wrong)"
    """

    data  = ""

    for x, y, isAdmitted, pred in zip( features[xlabel],
                                       features[ylabel],
                                       labels,
                                       predictions ):
        pIsAdmitted = pred["class_ids"][0]
        value = pIsAdmitted if pIsAdmitted == isAdmitted else pIsAdmitted + 2
        data  += f"{x} {y} {value}\n"

    data  += "e\n"
        
    sys.stdout.write(cmd + data)
