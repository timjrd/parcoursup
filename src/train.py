import const
import tf
# import random
# import time

def train(features, labels):
    # randomize.seed += 1
    
    featureColumns = [ tf.feature_column.numeric_column(key=k) for k in features.keys() ]

    # classifier = tf.estimator.LinearClassifier(
    #     feature_columns=featureColumns,
    #     n_classes=2 )
    
    classifier = tf.estimator.DNNClassifier(
        feature_columns=featureColumns,
        hidden_units=[10, 10, 10, 10],
        n_classes=2 )

    def inputFn():
        dataset = tf.data.Dataset.from_tensor_slices((features, labels))
        # [WARNING] shuffle(): see
        # <https://stackoverflow.com/questions/46444018/meaning-of-buffer-size-in-dataset-map-dataset-prefetch-and-dataset-shuffle>
        dataset = ( dataset
                    .shuffle(buffer_size=const.SHUFFLE_BUFFER_SIZE)
                    .repeat(count=None)
                    .batch(const.BATCH_SIZE) )
        return dataset.make_one_shot_iterator().get_next()

    classifier.train(
        input_fn=inputFn,
        steps=const.TRAIN_STEPS )

    return classifier

# def randomize(xs):
#     result = list(xs)
#     random.Random(randomize.seed).shuffle(result)
#     return result
# randomize.seed = time.time()
