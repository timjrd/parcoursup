import sys

def write(s):
    sys.stderr.write(s)

def writen(n,s):
    for _ in range(n):
        write(s)

def progress(x):
    size   = 40
    filled = int(x * size)
    blanks = size - filled
    
    write("\r[")    
    if filled < size:
        writen(filled-1, "=")
        if filled-1 > 0:
            write(">")
    else:
        writen(filled, "=")
    writen(blanks, " ")
    write("]")
    
    write(" {}% ".format(int(x*100)).rjust(6))
    sys.stderr.flush()
