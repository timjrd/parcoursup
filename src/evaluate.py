import const
import tf

def evaluate(features, labels, classifier):
    
    def inputFn():
        dataset = tf.data.Dataset.from_tensor_slices((features, labels))
        dataset = dataset.batch(const.BATCH_SIZE)
        return dataset.make_one_shot_iterator().get_next()

    return classifier.evaluate(input_fn=inputFn)
