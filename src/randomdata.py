import random
from collections import defaultdict

def emptyFeatures(target):
    result = {}
    for k,_ in target.items():
        result[k] = []
    return result

def concatFeatures(a,b):
    result = {}
    for k,v in a.items():
        result[k] = v + b[k]
    return result

def features(target, nbStudents):
    result = {}
    for k,_ in target.items():
        result[k] = [mark() for _ in range(nbStudents)]
    return result

def mark(mu=10, sigma=3, lower=0, upper=20):
    r = round(random.gauss(mu,sigma), 1)
    return max(lower, min(r, upper))

def labels(features, target, numerusClausus):
    nbStudents = len(next(iter(features.values())))
    result = []
    for i in range(nbStudents):
        marks = {}
        for k,v in features.items():
            marks[k] = v[i]
            
        s = score(marks, target)
        result.append((i,s))

    result.sort(key=lambda x: x[1], reverse=True)

    for i in range(numerusClausus):
        (a,b) = result[i]
        result[i] = (a, 1)
    for i in range(numerusClausus, nbStudents):
        (a,b) = result[i]
        result[i] = (a, 0)

    result.sort(key=lambda x: x[0])
    return list(map(lambda x: x[1], result))

def score(marks, target):
    result = 0
    for k,v in target.items():
        result += marks[k] * v
    return result

def admittedOnly(features, labels):
    nbStudents = len(next(iter(features.values())))
    newFeatures = defaultdict(list)
    newLabels   = []
    for i in range(nbStudents):
        if (labels[i] == 1):
            newLabels.append(1)
            for k,v in features.items():
                newFeatures[k].append(v[i])
    print("\n\nadmittedOnly={}\n\n".format(len(newLabels)))
    return dict(newFeatures), newLabels

def show(features, labels):
    nbStudents = len(next(iter(features.values())))
    for i in range(nbStudents):
        if (labels[i] == 1):
            print("ADMITTED ", end="")
            for k,v in features.items():
                print(k + "=" + str(v[i]) + " ", end="")
            print()
