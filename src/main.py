from const    import *
from train    import *
from evaluate import *
from predict  import *
import progress   as pg
import randomdata as rd
import plot
import sys

#target = {"math": 0.5, "english": 0.3, "french": 0.2}
target = {"math": 0.7, "english": 0.3}

minNbStudents  = 10
maxNbStudents  = 500
steps          = 20

kNumerusClausus = 30/100

testSize = 5000

sys.stderr.write("generating plots...\n");
plot.init("math", "english")

for i in range(0,steps):
    progress   = i / (steps-1) if steps > 1 else 0

    pg.progress(progress)
    
    nbStudents = int(minNbStudents + progress * (maxNbStudents - minNbStudents))
    
    numerusClausus = int(nbStudents * kNumerusClausus)
    
    trainFeatures = rd.features(target, nbStudents)
    trainLabels   = rd.labels(trainFeatures, target, numerusClausus)

    n = testSize // nbStudents
    testFeatures = rd.emptyFeatures(target)
    testLabels   = []
    for _ in range(n):
        features = rd.features(target, nbStudents)
        labels   = rd.labels(features, target, numerusClausus)
        testFeatures = rd.concatFeatures(testFeatures, features)
        testLabels   = testLabels + labels

    classifier  = train(trainFeatures, trainLabels)
    accuracy    = evaluate(testFeatures, testLabels, classifier)["accuracy"]
    predictions = predict(testFeatures, testLabels, classifier)

    plot.plot( "math", "english",
               nbStudents, numerusClausus, accuracy,
               testFeatures, testLabels, predictions )

pg.progress(1)
sys.stderr.write("\n")
sys.stderr.write(f'output written to "{PLOT_FILE}"\n');
