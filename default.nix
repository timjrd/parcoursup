let
  rev     = "ce0d9d638ded6119f19d87e433e160603683fb1b"; # nixos-18.03
  sha256  = "0na6kjk4xw6gqrn3a903yv3zfa64bspq2q3kd6wyf52y44j3s8sx";
  nixpkgs = fetchTarball {
    url = "https://github.com/NixOS/nixpkgs-channels/archive/${rev}.tar.gz";
    inherit sha256;
  };
in with import nixpkgs {};
stdenvNoCC.mkDerivation {
  name = "parcoursup";
  buildInputs = [
    gnuplot    
    (python36.withPackages (x: with x; [
      tensorflow
    ]))
  ];
}
